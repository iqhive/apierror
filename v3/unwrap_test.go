package apierror

import (
	"errors"
	"testing"
)

var ErrGluttony = errors.New("gluttony")
var ErrGreed = errors.New("greed")

func TestUnwrap(t *testing.T) {

	origin := ErrGluttony

	if !errors.Is(origin, ErrGluttony) {
		t.Fail()
	}

	ae := New(origin, 0, "")
	if !errors.Is(ae, ErrGluttony) {
		t.Fail()
	}

	ae2 := New(ae, 0, "")
	if !errors.Is(ae2, ErrGluttony) {
		t.Fail()
	}

	ae3 := New(origin, 0, "gluttony child")
	ae4 := New(ae3, 0, "parent of gluttony which is greed")
	// apply greed
	origin2 := ErrGreed
	ae4.ApplyForeignError(origin2, 429, "field")
	// Should still be the original "origin" error (ErrGluttony)
	if !errors.Is(ae4, ErrGluttony) {
		t.Fail()
	}
}
