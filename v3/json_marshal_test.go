package apierror

import (
	"bytes"
	"encoding/json"
	"errors"
	"testing"
)

func TestJSONBytes(t *testing.T) {
	DebugMode = false
	ApplicationName = "appname3"

	strErr := errors.New("String error 1")
	ae := NewAPIError(strErr, 400, "none", "test message 1")

	b, e := ae.GetJSONBytes()
	if e != nil {
		t.Fatalf("did not get json bytes: %v", e)
	}

	var got APIError

	if err := json.Unmarshal(b, &got); err != nil {
		t.Fatalf("did not unmarshal: %s", b)
	}

	c, e := got.GetJSONBytes()
	if e != nil {
		t.Fatalf("did not get json bytes: %v", e)
	}

	if !bytes.Equal(b, c) {
		t.Fatalf("unexpected differences between the original error and the error recreated from json:\n got: %s\nwant: %s", c, b)
	}

}
