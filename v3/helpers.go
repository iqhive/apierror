package apierror

import (
	"google.golang.org/grpc/status"
)

// EmptySlice is used for 404 slice replies
var EmptySlice = make([]NullStruct, 0)

// EmptyObject is used for 404 slice replies
var EmptyObject = NullStruct{}

// NullStruct is used for 404 object replies
type NullStruct struct {
}

var (
	ErrObjectNil      = &APIError{ErrorType: ErrorType_ERROR, ErrorCode: 500, ErrorMessage: "Object is nil"}
	ErrObjectNotFound = &APIError{ErrorType: ErrorType_INFO, ErrorCode: 404, ErrorMessage: "Object not found"}
)

func Is404Error(err error) bool {
	return GetErrorCode(err) == 404
}

func GetErrorCode(err error) int {
	if err == nil {
		return 0
	}

	if err == ErrObjectNotFound {
		return 404
	}

	if ae, ok := err.(*APIError); ok {
		if ae.ErrorCode == 404 || ae.originErr == ErrObjectNotFound {
			return 404
		}
		if ae.ErrorCode <= 0 {
			return 500
		}
		return ae.ErrorCode
	}

	if st, ok := status.FromError(err); ok {
		return GRPCCodeToHTTPCode(st.Code())
	}

	return 500
}
