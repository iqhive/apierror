package apierror

import (
	"testing"
)

func TestAPIErrorMessage(t *testing.T) {
	DebugMode = true

	ae := NewAPIError(nil, 400, "none", "test message")

	if ae.Error() != "test message" {
		t.Logf("Unexpected error message (%s)", ae.Error())
		t.Fail()
	}

}

func originErrEmptyMessage() error {
	p := NewAPIError(nil, 0, "", "")
	return p
}

func TestAPIErrorNoMessage(t *testing.T) {
	DebugMode = true
	e := originErrEmptyMessage()
	ae := NewAPIError(e, 402, "", "")

	if ae.Error() != "" {
		t.Log("Unexpected error message")
		t.Fail()
	}

}

type MyStructError struct{}

func (m MyStructError) Error() string {
	return "boom"
}

func TestAPIErrorReflectStruct(t *testing.T) {
	DebugMode = true
	str := MyStructError{}
	ae := NewAPIError(str, 0, "", "test message")

	if ae.Error() != "test message" {
		t.Log("Unexpected error message")
		t.Fail()
	}

	if ae.ErrorCode != 500 {
		t.Log("Unexpected ErrorCode")
		t.Fail()
	}

}

func originErrEmpty() error {
	return NewAPIError(nil, 0, "", "")
}

func TestAPIErrorZeroErrorCode(t *testing.T) {
	DebugMode = true

	e := originErrEmpty()
	ae := NewAPIError(e, 0, "", "test message")

	if ae.Error() != "test message" {
		t.Log("Unexpected error message")
		t.Fail()
	}

	if ae.ErrorCode != 500 {
		t.Log("Unexpected ErrorCode")
		t.Fail()
	}

}

func originErrEmptyWithCode() error {
	e := NewAPIError(nil, 499, "", "")
	return e
}

func TestAPIErrorErrorCodeNoMsg(t *testing.T) {
	DebugMode = true

	e := originErrEmptyWithCode()
	ae := NewAPIError(e, 0, "", "")

	if ae.Error() != "" {
		t.Log("Unexpected error message")
		t.Fail()
	}

	if ae.ErrorCode != 499 {
		t.Log("Unexpected ErrorCode")
		t.Fail()
	}

}

func originErrEmptyPointer() error {
	p := NewAPIError(nil, 0, "", "")
	return p
}

func TestAPIErrorZeroErrorCodePointer(t *testing.T) {
	DebugMode = true

	e := originErrEmptyPointer()
	ae := NewAPIError(e, 0, "", "test message")

	if ae.Error() != "test message" {
		t.Log("Unexpected error message")
		t.Fail()
	}

	if ae.ErrorCode != 500 {
		t.Log("Unexpected ErrorCode")
		t.Fail()
	}

}

func originErrEmptyWithCodePointer() error {
	p := NewAPIError(nil, 499, "", "")
	return p
}

func TestAPIErrorErrorCodeNoMsgPointer(t *testing.T) {
	DebugMode = true

	e := originErrEmptyWithCodePointer()
	ae := NewAPIError(e, 0, "", "")

	if ae.Error() != "" {
		t.Log("Unexpected error message")
		t.Fail()
	}

	if ae.ErrorCode != 499 {
		t.Log("Unexpected ErrorCode")
		t.Fail()
	}

}

func getErrMessage(e error) string {
	return e.Error()
}

func TestAPIErrorNilPointer(t *testing.T) {
	DebugMode = true
	aep := NilError
	str := getErrMessage(&aep)
	if str != "<nil>" {
		t.Fail()
	}
}

func TestAPIErrorNilPointer2(t *testing.T) {
	DebugMode = true
	var aep1 *APIError
	str := getErrMessage(aep1)
	if str != "<nil>" {
		t.Fail()
	}
}

type BadErrorType struct{}

func (BadErrorType) Error() string {
	return "nope"
}

func TestAPIErrorNilPointer3(t *testing.T) {
	DebugMode = true
	var bet *BadErrorType
	aep1 := NewAPIError(bet, 123, "", "123")
	if aep1.OriginErrorMessage != "" {
		t.Fail()
	}
}

func TestAPIErrorNilPointer4(t *testing.T) {
	DebugMode = true
	var bet *BadErrorType
	aep1 := NewAPIError(bet, 0, "", "")
	if aep1 == nil {
		t.Fail()
	}
	// str := getErrMessage(aep1)
	// if str != "<nil>" {
	// 	t.Fail()
	// }
}

func TestAPIError404(t *testing.T) {
	aep1 := NewAPIError(nil, 404, "test404", "not found")
	if !Is404Error(aep1) {
		t.Fail()
	}

	if GetErrorCode(aep1) != 404 {
		t.Fail()
	}
}

func TestAPIErrorIfErrorWithNil(t *testing.T) {
	aep1 := NewAPIErrorIfError(nil, 404, "test404", "not found")
	if aep1 != nil {
		t.Fail()
	}
	t.Logf("TestAPIErrorIfErrorWithNil: %v", aep1)
}

func TestAPIErrorPBBase64(t *testing.T) {
	//
	// Tests base 64 encoding
	//
	//
	ae := NewAPIError(nil, 404, "test404", "not found")
	b64String := ae.ToBase64String(true)
	t.Logf("(%s)", b64String)
	if b64String != "EJQDIgd0ZXN0NDA0Kglub3QgZm91bmRCIGFwaWVycm9yL3YzL2FwaWVycm9yX3Rlc3QuZ286MjAx" {
		t.Logf("Unexpected output (%s)", b64String)
		t.Fail()
	}
	// str := getErrMessage(aep1)
	// if str != "<nil>" {
	// 	t.Fail()
	// }
} // 204

func TestAPIErrorJSONDebug(t *testing.T) {
	DebugMode = true
	ae := NewAPIError(nil, 404, "test404", "not found")
	want := `{"err_code":404,"err_type":"ERROR","err_field":"test404","err_message":"not found","source_file":"apierror/v3/apierror_test.go:216","source_func":"v3.TestAPIErrorJSONDebug"}`
	got := ae.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorJSONNoDebug(t *testing.T) {
	DebugMode = false
	ae := NewAPIError(nil, 404, "test404", "not found")
	want := `{"err_code":404,"err_type":"ERROR","err_field":"test404","err_message":"not found","source_file":"apierror/v3/apierror_test.go:226","source_func":"v3.TestAPIErrorJSONNoDebug"}`
	got := ae.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorNoType200(t *testing.T) {
	DebugMode = false
	ae := New(nil, 200, "wrong data")
	want := `{"err_code":200,"err_type":"INFO","err_message":"wrong data","source_file":"apierror/v3/apierror_test.go:236","source_func":"v3.TestAPIErrorNoType200"}`
	got := ae.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorNoType400(t *testing.T) {
	DebugMode = false
	ae := New(nil, 400, "wrong data")
	want := `{"err_code":400,"err_type":"ERROR","err_message":"wrong data","source_file":"apierror/v3/apierror_test.go:246","source_func":"v3.TestAPIErrorNoType400"}`
	got := ae.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorNoType404(t *testing.T) {
	DebugMode = false
	ae := New(nil, 404, "no data")
	want := `{"err_code":404,"err_type":"INFO","err_message":"no data","source_file":"apierror/v3/apierror_test.go:256","source_func":"v3.TestAPIErrorNoType404"}`
	got := ae.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorInteritTypeWithNew(t *testing.T) {
	DebugMode = false
	aechild := New(nil, 404, "no data")
	aeparent := New(aechild, 0, "upper error")
	want := `{"err_code":404,"err_type":"INFO","err_message":"upper error","source_file":"apierror/v3/apierror_test.go:267","source_func":"v3.TestAPIErrorInteritTypeWithNew","stack":{"err_code":404,"err_type":"INFO","err_message":"no data","source_file":"apierror/v3/apierror_test.go:266","source_func":"v3.TestAPIErrorInteritTypeWithNew"}}`
	got := aeparent.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorInteritTypeWithNewAPIError(t *testing.T) {
	DebugMode = false
	aechild := New(nil, 404, "no data")
	aeparent := NewAPIError(aechild, 0, "", "upper error")
	want := `{"err_code":404,"err_type":"ERROR","err_message":"upper error","source_file":"apierror/v3/apierror_test.go:278","source_func":"v3.TestAPIErrorInteritTypeWithNewAPIError","stack":{"err_code":404,"err_type":"INFO","err_message":"no data","source_file":"apierror/v3/apierror_test.go:277","source_func":"v3.TestAPIErrorInteritTypeWithNewAPIError"}}`
	got := aeparent.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}

func TestAPIErrorInteritTypeWithNewAPIWarning(t *testing.T) {
	DebugMode = false
	aechild := New(nil, 404, "no data")
	aeparent := NewAPIWarning(aechild, 0, "", "upper error")
	want := `{"err_code":404,"err_type":"WARNING","err_message":"upper error","source_file":"apierror/v3/apierror_test.go:289","source_func":"v3.TestAPIErrorInteritTypeWithNewAPIWarning","stack":{"err_code":404,"err_type":"INFO","err_message":"no data","source_file":"apierror/v3/apierror_test.go:288","source_func":"v3.TestAPIErrorInteritTypeWithNewAPIWarning"}}`
	got := aeparent.GetJSONString()
	if got != want {
		t.Fatalf("Unexpected output:\nwant: %s\n got: %s\n", want, got)
	}
}
