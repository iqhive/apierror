package apierror

import (
	"context"

	iqlog "bitbucket.org/iqhive/iqlog/v3"
)

const (
	// DefaultLogFormatJSONNewLine is used to select the JSON log format
	DefaultLogFormatJSONNewLine = 0
	// DefaultLogFormatLineLog is used to select the line log format (default)
	DefaultLogFormatLineLog = 1
	// DefaultLogFormatJSON is used to select the JSON log formatter
	DefaultLogFormatJSON = 2
)

// APILogger defines an APIError logging interface. This can be used to define your own logging mechanism
type APILogger interface {
	// HandleLogMessage(ae *APIError)
	HandleLogMessageWithContext(ctx context.Context, ae *APIError)
}

// APILoggerHandler is a convenience type to avoid having to declare a struct
// to implement the APILogger interface, it can be used like this:
//
//	apierror.Logger = apierror.APILoggerHandler(func(ae APIError) {
//		// handle the log message
//	})
type APILoggerHandler func(ctx context.Context, ae *APIError)

// // HandleLogMessage implements the Handler interface for the above-mentioned APILoggerHandler convenience type
// func (h APILoggerHandler) HandleLogMessage(ae *APIError) {
// 	if ae != nil {
// 		h(nil, ae)
// 	}
// }

// HandleLogMessageWithContext implements the Handler interface for the above-mentioned APILoggerHandler convenience type
func (h APILoggerHandler) HandleLogMessageWithContext(ctx context.Context, ae *APIError) {
	if ae != nil {
		h(ctx, ae)
	}
}

type DefaultAPILoggerHandler struct{}

func (obj *DefaultAPILoggerHandler) HandleLogMessageWithContext(ctx context.Context, ae *APIError) {
	if obj == nil || ae == nil || iqlog.GlobalLogger == nil {
		return
	}
	fields := ae.GetMapStringInterface()
	switch ae.ErrorType {
	case ErrorType_WARNING:
		iqlog.GlobalLogger.WithFields(fields).Warn(ae.ErrorMessage)

	case ErrorType_INFO:
		iqlog.GlobalLogger.WithFields(fields).Info(ae.ErrorMessage)

	case ErrorType_DEBUG:
		iqlog.GlobalLogger.WithFields(fields).Debug(ae.ErrorMessage)

	default:
		// same as ErrorType_ERROR, ErrorType_UNKNOWN:
		iqlog.GlobalLogger.WithFields(fields).Error(ae.ErrorMessage)

	}
}
