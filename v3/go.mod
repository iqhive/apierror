module bitbucket.org/iqhive/apierror/v3

go 1.23

require (
	bitbucket.org/iqhive/iqlog/v3 v3.0.12
	google.golang.org/genproto v0.0.0-20220314164441-57ef72a4c106
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/crypto v0.26.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.23.0 // indirect
	golang.org/x/term v0.23.0 // indirect
	golang.org/x/text v0.17.0 // indirect
)
