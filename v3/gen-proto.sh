#!/bin/bash
#  -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/ \

protoc ./apierror.proto \
  -I . \
  -I $GOPATH/src/ \
  -I $GOPATH/src/github.com/googleapis/googleapis/ \
  -I /usr/local/include/ \
  --go_opt=paths=source_relative \
  --go_out=./ \
  --go-grpc_out=./
