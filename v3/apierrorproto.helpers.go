package apierror

import (
	"bytes"
	"compress/flate"
	"encoding/base64"
	"io/ioutil"
	"math/rand"

	"google.golang.org/protobuf/proto"
)

// FromPackedDataBase64 returns the APIError object encoded in Protobuf
func (ae *APIError) FromPackedDataBase64(in string) error {
	if len(in) == 0 {
		return nil
	}
	packedBytes, err := base64.RawURLEncoding.DecodeString(in)
	if err != nil {
		return NewAPIError(err, 500, "", "Unable to decode APIError message")
	}

	if err := ae.FromPackedData(packedBytes); err != nil {
		return NewAPIError(err, 500, "", "")
	}

	return nil
}

// FromPackedDataBase64 returns the APIError object encoded in Protobuf
func (aep *APIErrorProto) FromPackedDataBase64(in string) error {
	if len(in) == 0 {
		return nil
	}
	packedBytes, err := base64.RawURLEncoding.DecodeString(in)
	if err != nil {
		return NewAPIError(err, 500, "", "Unable to decode APIErrorProto message")
	}

	if err := aep.FromPackedData(packedBytes); err != nil {
		return NewAPIError(err, 500, "", "")
	}

	return nil
}

// FromPackedData returns the APIError object encoded in Protobuf
func (ae *APIError) FromPackedData(b []byte) error {
	aep := &APIErrorProto{}
	if err := aep.FromPackedData(b); err != nil {
		return NewAPIError(err, 500, "", "")
	}

	ae.FromAPIErrorProto(aep)

	return nil
}

// FromPackedData returns the APIErrorProto object encoded in Protobuf
func (aep *APIErrorProto) FromPackedData(b []byte) error {
	if aep == nil {
		return nil
	}

	data := bytes.NewReader(b)
	r := flate.NewReader(data)

	uncompressedProtoBytes, err := ioutil.ReadAll(r)
	if err != nil {
		return NewAPIError(err, 500, "", "")
	}

	err = proto.Unmarshal(uncompressedProtoBytes, aep)
	if err != nil {
		return NewAPIError(err, 500, "", "")
	}

	return nil
}

// ToPackedDataBase64 returns the APIError object encoded in Protobuf
func (ae *APIError) ToPackedDataBase64(saveSpace bool) (string, error) {
	packedBytes, err := ae.ToPackedData(saveSpace)
	if err != nil {
		return "", NewAPIError(err, 500, "", "")
	}

	return base64.RawURLEncoding.EncodeToString(packedBytes), nil
}

// ToPackedDataBase64 returns the APIErrorProto object encoded in Protobuf
func (aep *APIErrorProto) ToPackedDataBase64() (string, error) {
	packedBytes, err := aep.ToPackedData()
	if err != nil {
		return "", NewAPIError(err, 500, "", "")
	}

	return base64.RawURLEncoding.EncodeToString(packedBytes), nil
}

// ToPackedData returns the APIErrorProto object encoded in Protobuf
func (ae *APIError) ToPackedData(saveSpace bool) ([]byte, error) {
	aep := APIErrorProto{}
	aep.FromAPIError(ae, saveSpace)
	b, err := aep.ToPackedData()
	if err != nil {
		return nil, NewAPIError(err, 500, "", "")
	}

	return b, nil
}

// ToPackedData returns the APIErrorProto object encoded in Protobuf
func (aep *APIErrorProto) ToPackedData() ([]byte, error) {
	var (
		buf bytes.Buffer
		n   int
		// s       time.Time
		// elapsed time.Duration
		err error
		obj []byte
	)

	protoBytes, err := proto.Marshal(aep)
	if err != nil {
		return nil, NewAPIError(err, 500, "", "")
	}

	// fmt.Printf("size of protobuf: %d\n\n", len(protoBytes))

	buf.Reset()
	fw, err := flate.NewWriter(&buf, 9)
	if err != nil {
		return nil, NewAPIError(err, 500, "", "")
	}
	defer fw.Close()

	// s = time.Now()
	n, err = fw.Write(protoBytes)
	if err != nil {
		return nil, NewAPIError(err, 500, "", "")
	}
	_ = n
	fw.Close() // need to explicitly close and not just flush to prevent EOF error
	// elapsed = time.Since(s)

	obj = buf.Bytes()
	// fmt.Printf("written to flated: %d\n", n)
	// fmt.Printf("size of flated: %d\n", len(obj))
	// fmt.Printf("elapsed flated: %v\n\n", elapsed)

	return obj, nil

	// err = decompressFlate(obj, aep)
	// if err != nil {
	// 	return nil, NewAPIError(err, 500, "", "")
	// }

	// buf.Reset()
	// zw := zlib.NewWriter(&buf)
	// defer zw.Close()

	// s = time.Now()
	// n, err = zw.Write(protoBytes)
	// if err != nil {
	// 	return nil, NewAPIError(err, 500, "", "")
	// }
	// zw.Flush()
	// elapsed = time.Since(s)

	// obj = buf.Bytes()
	// fmt.Printf("written to zipped: %d\n", n)
	// fmt.Printf("size of zlib: %d\n", len(obj))
	// fmt.Printf("elapsed zlib: %v\n\n", elapsed)

	// buf.Reset()
	// gw := gzip.NewWriter(&buf)
	// defer gw.Close()

	// s = time.Now()
	// n, err = gw.Write(protoBytes)
	// if err != nil {
	// 	return nil, NewAPIError(err, 500, "", "")
	// }
	// gw.Flush()
	// elapsed = time.Since(s)

	// obj = buf.Bytes()
	// fmt.Printf("written to gzipped: %d\n", n)
	// fmt.Printf("size of gzip: %d\n", len(obj))
	// fmt.Printf("elapsed gzip: %v\n\n", elapsed)

	// return protoBytes, nil
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// func decompressFlate(obj []byte, expected *APIErrorProto) error {
// 	fmt.Println("Decompressing")

// 	data := bytes.NewReader(obj)
// 	r := flate.NewReader(data)

// 	enflate, err := ioutil.ReadAll(r)
// 	if err != nil {
// 		return NewAPIError(err, 500, "", "")
// 	}

// 	result := APIErrorProto{}
// 	proto.Unmarshal(enflate, &result)

// 	if result.Message != expected.Message {
// 		fmt.Printf("RESULT[%s] != EXPECTED[%s]\n", result.Message, expected.Message)
// 		return errors.New("not equal")
// 	}

// 	fmt.Println("DECOMPRESSION EQUAL")
// 	return nil
// }

// GetPBBytes returns the APIErrorProto object encoded in Protobuf
func (ae *APIErrorProto) GetPBBytes() ([]byte, error) {
	return proto.Marshal(ae)
}

// GetAPIError returns the APIError object
func (aep *APIErrorProto) GetAPIError() *APIError {
	ae := APIError{}
	ae.FromAPIErrorProto(aep)
	return &ae
}

// FromAPIError loads an APIError into an APIErrorProto
func (aep *APIErrorProto) FromAPIError(ae *APIError, saveSpace bool) {
	if ae == nil || aep == nil {
		return
	}
	aep.App = ae.App
	aep.ErrorCode = int32(ae.ErrorCode)
	aep.ErrorNumber = int32(ae.ErrorNumber)
	aep.ErrorField = ae.ErrorField
	aep.ErrorMessage = ae.ErrorMessage
	aep.SourceFile = ae.SourceFile
	if !saveSpace {
		aep.ErrorType = ae.ErrorType
		aep.SourceFunc = ae.SourceFunc
		aep.OriginErrorMessage = ae.OriginErrorMessage
	}

	for i := range ae.traceFrames {
		aep.TraceFrames = append(aep.TraceFrames, &ae.traceFrames[i])
	}

	if ae.Stack != nil {
		stackAEP := APIErrorProto{}
		stackAEP.FromAPIError(ae.Stack, saveSpace)
		aep.Stack = &stackAEP
	}
}

// FromAPIErrorProto loads an APIErrorProto into an APIError
func (ae *APIError) FromAPIErrorProto(aep *APIErrorProto) {
	if ae == nil || aep == nil {
		return
	}
	ae.App = aep.App
	ae.ErrorCode = int(aep.ErrorCode)
	ae.ErrorNumber = int(aep.ErrorNumber)
	ae.ErrorType = aep.ErrorType
	ae.ErrorField = aep.ErrorField
	ae.ErrorMessage = aep.ErrorMessage
	ae.OriginErrorMessage = aep.OriginErrorMessage
	ae.SourceFile = aep.SourceFile
	ae.SourceFunc = aep.SourceFunc

	for i := range aep.TraceFrames {
		if aep.TraceFrames[i] == nil {
			continue
		}
		ae.traceFrames = append(ae.traceFrames, *aep.TraceFrames[i])
	}

	if aep.Stack != nil {
		stackAE := APIError{}
		stackAE.FromAPIErrorProto(aep.Stack)
		ae.Stack = &stackAE
	}
}
