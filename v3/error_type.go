package apierror

import (
	"strings"
)

// UnmarshalJSON unmarshals JSON into the ErrorType object
func (obj *ErrorType) UnmarshalJSON(data []byte) error {
	if v, ok := ErrorType_value[StripStringQuotes(strings.ToUpper(string(data)))]; ok {
		*obj = ErrorType(v)
		return nil
	}
	return NewAPIError(nil, 400, "ErrorType", "Invalid ErrorType Unmarshal value (%v) from (%s)", *obj, string(data))
}

// MarshalJSON marshals the ErrorType object into JSON
func (obj ErrorType) MarshalJSON() ([]byte, error) {
	if obj == 0 {
		return []byte("\"\""), nil
	}
	if v, ok := ErrorType_name[int32(obj)]; ok {
		return []byte("\"" + v + "\""), nil
	}
	return nil, NewAPIError(nil, 400, "ErrorType", "Invalid ErrorType Marshal value from (%d)", int32(obj))
}

// StripStringQuotes removes the string quotes from a JSON variable
func StripStringQuotes(in string) string {
	if len(in) >= 2 && in[0] == byte('"') && in[len(in)-1] == byte('"') {
		return in[1 : len(in)-1]
	}
	return in
}
